#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import datetime as dt
import sys
from PyQt4 import QtGui, QtCore
import numpy as np
import math

from random import randint

"""
common functions
"""


def get_file_format(fn):
    if fn is not None:
        frm = fn.split("/")[-1].split(".")[-1]
        return frm
    else:
        return None


"""
Image pre-processing functions using OpenCV
    Open/save
"""


def get_ipl(fp, new=False, fn=None, fl=None):
    if fp is not None:
        fp = str(fp).replace("/", "//")
        frm = fp.split("/")[-1].split(".")[-1]
        if frm in fl:
            try:
                img = im_bgr2rgb(cv2.imread(fp))
                if new:
                    print "    File \"{0}\" successfully opened".format(fn)
                return img
            except Exception as e:
                print "Try to get_ipl: ", e.arggs, e.message
        else:
            print "Can't show image \"{0}\". Reason: Wrong file format:\"{1}\"".format(str(fn.split("/")[-1]), frm)
    else:
        print "Nothing to open"
        return None


def im_save(img, n=1):
    cur_time = dt.datetime.now()
    cur_data = str(cur_time).split()[0]
    cur_time = str(cur_time).split()[1].split(".")[0].replace(":", "_")
    if str(sys.platform)[0:3] == "lin":
        fname = "./generated_images/image{0}_{1}-{2}.jpg".format(str(n), cur_data, cur_time)
    elif str(sys.platform)[0:3] == "win":
        fname = "./generated_images/image{0}_{1}-{2}.jpg".format(str(n), cur_data, cur_time)
    print fname
    cv2.imwrite(fname, img)

"""
    filter
"""


def median_blur(img, k=5):
    blur = cv2.medianBlur(img, k)
    return blur

"""
    channel conversion
"""


def to_gray(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray


def im_bgr2rgb(bgr):
    rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
    return rgb


def im_gray2rgb(gray):
    rgb = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
    return rgb

"""
    resizing
"""


def im_res(image, width=None, height=None, inter=cv2.INTER_AREA):
    (h, w) = image.shape[:2]
    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        d = (int(w * r), height)
    else:
        r = width / float(w)
        d = (width, int(h * r))
    res_img = cv2.resize(image, d, interpolation=inter)
    return res_img


"""
QT setup functions
    get pixel map from OpenCV's IplImage
"""


def setup_pix_map(img):
    if len(img.shape) > 2:
        """If RGB"""
        h, w = img.shape[:2]
        bp_line = 3 * w
    else:
        """If Gray"""
        img2 = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        h, w = img2.shape[:2]
        bp_line = w
    if img is not None:
        """bpline = byte per line"""

        """QImage from IplImage to PixMap"""
        qim = QtGui.QImage(img.data, w, h, bp_line, QtGui.QImage.Format_RGB888)
        pm = QtGui.QPixmap.fromImage(qim)
        return pm


"""
    Show OpenCV's IplImage in QtLabel
"""


def show_img(img=None, container=QtGui.QLabel, default=lambda: None, size=200):
    if img is not None:
        container.setPixmap(setup_pix_map(im_res(img, size)))
    else:
        print "Nothing to show"
        default()

"""
Checking available web-cams
"""


def get_cams(list_of_cams=None, n=0):
    for i in xrange(10):
        try:
            t_cap = cv2.VideoCapture(i)
            k = t_cap.isOpened()
            list_of_cams[i] = k
        except Exception as e:
            print "Get cams: ", e.args, e.message
    list_of_cams = [i for i in list_of_cams if list_of_cams[i]]
    print "Cams {0} are free to use".format(list_of_cams)
    t_cap.release()
    if len(list_of_cams) > 0:
        n = list_of_cams[0]
    return n


"""
Numpy array rotation
"""


def rotate_image(image, angle):
    """
    Rotates an OpenCV 2 / NumPy image about it's centre by the given angle
    (in radians). The returned image will be large enough to hold the entire
    new image, with a black background
    """
    # Get the image size
    # No that's not an error - NumPy stores image matricies backwards
    image_size = (image.shape[1], image.shape[0])
    image_center = tuple(np.array(image_size) / 2)
    # Convert the OpenCV 3x2 rotation matrix to 3x3
    rot_mat = np.vstack(
        [cv2.getRotationMatrix2D(image_center, angle, 1.0), [0, 0, 1]]
    )

    rot_mat_no_translate = np.matrix(rot_mat[0:2, 0:2])

    # Shorthand for below calcs
    image_w2 = image_size[0] * 0.5
    image_h2 = image_size[1] * 0.5

    # Obtain the rotated coordinates of the image corners
    rotated_coord_list = [
        (np.array([-image_w2,  image_h2]) * rot_mat_no_translate).A[0],
        (np.array([ image_w2,  image_h2]) * rot_mat_no_translate).A[0],
        (np.array([-image_w2, -image_h2]) * rot_mat_no_translate).A[0],
        (np.array([ image_w2, -image_h2]) * rot_mat_no_translate).A[0]
    ]

    # Find the size of the new image
    x_coord_list = [pt[0] for pt in rotated_coord_list]
    x_pos = [x for x in x_coord_list if x > 0]
    x_neg = [x for x in x_coord_list if x < 0]

    y_coord_list = [pt[1] for pt in rotated_coord_list]
    y_pos = [y for y in y_coord_list if y > 0]
    y_neg = [y for y in y_coord_list if y < 0]

    right_bound = max(x_pos)
    left_bound = min(x_neg)
    top_bound = max(y_pos)
    bot_bound = min(y_neg)

    new_w = int(abs(right_bound - left_bound))
    new_h = int(abs(top_bound - bot_bound))

    # We require a translation matrix to keep the image centred
    trans_mat = np.matrix([
        [1, 0, int(new_w * 0.5 - image_w2)],
        [0, 1, int(new_h * 0.5 - image_h2)],
        [0, 0, 1]
    ])

    # Compute the tranform for the combined rotation and translation
    affine_mat = (np.matrix(trans_mat) * np.matrix(rot_mat))[0:2, :]

    # Apply the transform
    result = cv2.warpAffine(
        image,
        affine_mat,
        (new_w, new_h),
        flags=cv2.INTER_LINEAR
    )

    return result


def largest_rotated_rect(w, h, angle):
    quadrant = int(math.floor(angle / (math.pi / 2))) & 3
    sign_alpha = angle if ((quadrant & 1) == 0) else math.pi - angle
    alpha = (sign_alpha % math.pi + math.pi) % math.pi

    bb_w = w * math.cos(alpha) + h * math.sin(alpha)
    bb_h = w * math.sin(alpha) + h * math.cos(alpha)

    gamma = math.atan2(bb_w, bb_w) if (w < h) else math.atan2(bb_w, bb_w)

    delta = math.pi - alpha - gamma

    length = h if (w < h) else w

    d = length * math.cos(alpha)
    a = d * math.sin(alpha) / math.sin(delta)

    y = a * math.cos(gamma)
    x = y * math.tan(gamma)

    return (
        bb_w - 2 * x,
        bb_h - 2 * y
    )


def crop_around_center(image, width, height):
    image_size = (image.shape[1], image.shape[0])
    image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))
    if width > image_size[0]:
        width = image_size[0]
    if height > image_size[1]:
        height = image_size[1]
    x1 = int(image_center[0] - width * 0.5)
    x2 = int(image_center[0] + width * 0.5)
    y1 = int(image_center[1] - height * 0.5)
    y2 = int(image_center[1] + height * 0.5)
    return image[y1+2:y2, x1+2:x2]


def get_rotated_image(img, angle=0):
    if img is not None:
        image = img.copy()
        image_height, image_width = image.shape[0:2]
        image_rotated = rotate_image(image, angle)
        image_rotated_cropped = crop_around_center(
            image_rotated,
            *largest_rotated_rect(
                image_width,
                image_height,
                math.radians(angle)
            )
        )
        # cv2.imshow("Cropped Image", image_rotated_cropped)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        return image_rotated_cropped


"""
Set table data
"""


def set_table(container=None, row_count=1, col_count=6, table_data=None, headers=None):
    if container and table_data and headers:
        table = container
        table.clear()
        table.setRowCount(row_count)
        table.setColumnCount(col_count)
        data = table_data
        headers = headers
        for n, key in enumerate(headers):
            for k in data:
                new_item = QtGui.QTableWidgetItem(str(data[k][n]))
                table.setItem(k, n, new_item)
        table.setHorizontalHeaderLabels(headers)
        table.resizeColumnsToContents()
        table.resizeRowsToContents()
        print "Table successfully filled"
        return table
    else:
        print "Not enough data to fill the table"


"""
Calculate difference between 2 lists:
    1) List of features of etalons
    2) list of model features
"""


def get_pairs(new_features_list=None, model_list=None):
    if new_features_list and model_list:
        pairs = list(zip(new_features_list, model_list))
    return pairs


def get_dist(pairs, t=0.01):
    """
    Logic of how to calculate distance between 2 values if here
    t- is a theshold for feature distance calculation
    """
    dist = []
    estimates = []

    for p in pairs:
        d = abs(p[0]-p[1])
        dist.append(round(d, 2))
        if round(d, 2)-round(t, 2) == 0.0:
            estimates.append(1)
        else:
            estimates.append(0)
    estimates_sum = sum(estimates)
    return dist, estimates, len(estimates), estimates_sum


def get_bins(obj=None, etals=None):
    """
    lop - list of pairs:
        It is a list of lists.
        Each element of parent list is a list of pairs of feature values.
        Each pair consist of etalon and current object features.
        example:
        obj = [0.11, 0.02, 0.33, 0.44]
        etals = [
                [0.12, 0.03, 0.32, 0.1],
                [0.22, 0.03, 0.31, 0.3],
                [0.32, 0.03, 0.2, 0.4],
                [0.12, 0.02, 0.34, 0.1],
                [0.22, 0.01, 0.00, 0.2],
                [0.10, 0.00, 0.10, 0.2]]
        lop = [
                [(0.12,0.11), (0.03,0.02), (0.32,0.33), (0.1,0.44)],
                ...
                ]

    loe - list of estimates
    each element consists of   (list of distances,
                                bin list of estimates,
                                max value of estimates (count of features)
                                current object estimate of object)

        loe:
            [
            ([0.01, 0.01, 0.01, 0.34], [1, 1, 1, 0], 4, 3),
            ([0.11, 0.01, 0.02, 0.14], [0, 1, 0, 0], 4, 1),
            ([0.21, 0.01, 0.13, 0.04], [0, 1, 0, 0], 4, 1),
            ([0.01, 0.0, 0.01, 0.34], [1, 0, 1, 0], 4, 2),
            ([0.11, 0.01, 0.33, 0.24], [0, 1, 0, 0], 4, 1),
            ([0.01, 0.02, 0.23, 0.24], [1, 0, 0, 0], 4, 1)
            ]

    """
    if type(obj) is list and type(etals) is list and obj is not None and etals is not None:
        lop = []
        for i in etals:
            lop.append(get_pairs(i, obj))
        loe = []
        for i in lop:
            loe.append(get_dist(i))
        return loe


def test():
    obj = [0.11, 0.02, 0.33, 0.44]
    etals = [
            [0.12, 0.03, 0.32, 0.1],
            [0.22, 0.03, 0.31, 0.3],
            [0.32, 0.03, 0.2, 0.4],
            [0.12, 0.02, 0.34, 0.1],
            [0.22, 0.01, 0.00, 0.2],
            [0.10, 0.00, 0.10, 0.2]]
    for i in get_bins(obj=obj, etals=etals):
        print i

# test()
"""
random rotate generator
"""


def gen_rotate(img_list):
    """
    tmp_field - 1000х1000 white image with 1 randomly located object for correct contour calculating
    random_field - result field which contains all images
    """
    cnt_list = []

    if not img_list:
        return None
    h_max = 0
    for img in img_list[1:]:
        tmp = img.shape[0] if img.shape[0] > img.shape[1] else img.shape[1]
        h_max = tmp if tmp > h_max else h_max
    random_field = np.ones((h_max, h_max), np.uint8) * 255
    # random_field = np.ones((1500, 1500), np.uint8) * 255

    for img in img_list[1:]:
        angle = randint(0, 360)
        img = get_rotated_image(img, angle)
        tmp_field = np.ones(random_field.shape, np.uint8) * 255
        # img = utils.im_res(img, img.shape[0])
        h, w = img.shape[:2]

        rnd_x = randint(h/2+1, tmp_field.shape[0]-h/2-1)
        rnd_y = randint(h/2+1, tmp_field.shape[0]-h/2-1)
        tmp_field[rnd_y-h/2.0:rnd_y+h/2.0, rnd_x-w/2.0:rnd_x+w/2.0] = to_gray(img)

        contour, h = cv2.findContours(tmp_field.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnt_list.append(contour[0])

    if len(cnt_list[0]) < 5:
        cnt_list = cnt_list[1:]
    for cnt in cnt_list:
        color = randint(0, 150)
        cv2.drawContours(random_field, [cnt], -1, color, -1)
    return random_field


"""
Check pairs of projections
(Y, X)
"""


def get_olm(xy_list=None):
    """
    olm - object location matrix
    xy_list - coordinates of objects
    vhs - vector of horizontal sequence
    vvs - vector of vertical sequence
    """

    if not xy_list:
        return

    ob_num = len(xy_list)

    ol_mat = np.zeros((ob_num, ob_num), dtype=np.int)

    vhs = range(ob_num)
    vvs = range(ob_num)

    def sort_by_x(input_str):
        return input_str[0]

    def sort_by_y(input_str):
        return input_str[1]

    xy_by_x = xy_list[:]
    xy_by_x.sort(key=sort_by_x)
    xy_by_y = xy_list[:]
    xy_by_y.sort(key=sort_by_y)

    for n, i in enumerate(xy_by_y):
        index = xy_by_x.index(i)
        vvs[index] = n

    for i in xrange(ob_num):
        ol_mat[vvs[i]][vhs[i]] = 1

    return ol_mat


def test_check_pairs():
    orthogonal_centers = [(1, 0), (5, 3), (2, 2), (0, 4)]
    oblique_centers = [(1, 0), (3, 2), (5, 4), (0, 5)]

    object_a = get_olm(orthogonal_centers)
    object_b = get_olm(oblique_centers)
    print object_a
    print object_b

# test_check_pairs()