# -*- coding: utf-8 -*-
"""
keywords: telegram bot, image processing, delaunay triangulation, face analysis
references: https://github.com/szastupov/aiotg - telegram bot API + asyncio
            https://www.learnopencv.com/about/ - OpenCV examples and tutorials ( C++ / Python )
"""
from aiotg import Bot
import cv2
from telegram_bot.face_utils import delaunay_image, get_face_points
import urllib.request
from datetime import datetime, timezone
from telegram_bot.logger import logging
import json

log = logging.getLogger("PHOTO_BOT")

BOT_TOKEN = "330700543:AAEgYVmRIFBEj6JkarAn7ufN4Wx6fIEqOb0"

photobot = Bot(api_token=BOT_TOKEN)


def get_kb():
    kb = [['/p - get photo'],
          ['/d - get delaunay'],
          ['/t - get face dots'],
          ['/help']
          ]

    keyboard = {
        "keyboard": kb,
        "resize_keyboard": True
    }
    return keyboard

@photobot.command(r"(/?help)")
async def show_help(chat, match):
    help = '/p - get photo\n/d - get delaunay\n/t - get face dots'
    await chat.send_text(help, reply_markup=json.dumps(get_kb()))

@photobot.command(r"/(.)")
async def get_url(chat, match):
    user_id, user_name, user_last_name = get_user_info(chat)
    log.info("{}:[User: {} {}][id: {}][action: asked for a photo]".format(get_time(),
                                                        user_name,
                                                        user_last_name,
                                                        user_id
                                                        )
    )
    if match.string.split('/')[-1].split(' ')[0] not in ("p", "d", "t"):
        await chat.send_text("Не знаю такую комманду", reply_markup=json.dumps(get_kb()))
        return
    await chat.send_text("Подключаемся к камере...")
    try:
        cap = cv2.VideoCapture(0)
    except Exception as e:
        await chat.send_text("Ошибка подключаниея к камере...", reply_markup=json.dumps(get_kb()))
        print(e.args)
    await chat.send_text("Получаем кадр...")
    ret, frame = cap.read()
    if match.group(1) in ("p",):
        await chat.send_text("Возвращаем кадр...")
        cv2.imwrite("frame.png", frame)
    elif match.group(1) in ("t",):
        await chat.send_text("Находим ключевые точки...")
        try:
            frame = get_face_points(frame)
            cv2.imwrite("frame.png", frame)
        except Exception as e:
            print(e.args)
            await chat.send_text("Что-то пошло не так...")
        cap.release()
        await chat.send_text("Возвращаем кадр...")
    else:
        await chat.send_text("Находим ключевые точки...")
        try:
            frame = delaunay_image(frame)
            cv2.imwrite("frame.png", frame)
        except Exception as e:
            print(e.args)
            await chat.send_text("Что-то пошло не так...")
        cap.release()
        await chat.send_text("Возвращаем кадр...")
    with open("frame.png", 'rb') as f:
        img = f.read()
    await chat.send_photo(img, caption="Look at my face. Why so serious?", reply_markup=json.dumps(get_kb()))

@photobot.handle("photo")
async def handle(chat, photo):
    operations = {'t': lambda image: get_face_points(image),
                  'd': lambda image: delaunay_image(image)}
    user_id, user_name, user_last_name = get_user_info(chat)
    log.info("{}:[User: {} {}][id: {}][action: uploaded a photo]".format(get_time(),
                                                        user_name,
                                                        user_last_name,
                                                        user_id
                                                        )
    )
    file_id = photo[-1]['file_id']
    file = await photobot.get_file(file_id)
    img_url = "https://api.telegram.org/file/bot{}/{}".format(BOT_TOKEN, file['file_path'])
    urllib.request.urlretrieve(img_url, "test.jpg")
    await chat.send_text("Загружаем изображение")
    img = cv2.imread("test.jpg")

    await chat.send_text("Находим ключевые точки...")
    try:
        if chat.message.get('caption') in ('t', 'd',):
            dl_img = operations[chat.message.get('caption')](img)
        else:
            dl_img = get_face_points(img)
        cv2.imwrite("test.jpg", dl_img)
        with open("test.jpg", 'rb') as f:
            img = f.read()
    except Exception as e:
        print(e.args)
        await chat.send_text("Что-то пошло не так...", reply_markup=json.dumps(get_kb()))
    await chat.send_text("Возвращаем оббработанное изображение")
    await chat.send_photo(img, caption="Look at your picture.", reply_markup=json.dumps(get_kb()))


def get_user_info(chat):
    user_id = chat.message['chat']['id']
    user_name = chat.message['chat']['first_name']
    user_last_name = chat.message['chat']['last_name']
    return user_id, user_name, user_last_name

def get_time():
    time_format = "[%Y-%m-%d|%H:%M:%S]"
    now = datetime.now(timezone.utc).astimezone().strftime(time_format)
    return now

photobot.run()
