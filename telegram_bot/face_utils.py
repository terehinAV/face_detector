import dlib
import cv2
import numpy


def get_points(image, det, pred):
    faces = det(image, 1)
    p_dict = {}

    face_points = {}
    if len(faces) > 0:
        for k, d in enumerate(faces):
            point_list = []

            '''get points'''
            shape = pred(image, d)
            '''get coordinates in numpy array'''

            p_dict[k] = numpy.matrix([[p.x, p.y] for p in shape.parts()])
            '''draw points'''
            for n, p in enumerate(p_dict[k]):
                point = numpy.array(p).reshape(-1,).tolist()
                x, y = point[1], point[0]
                point_list.append((y, x))
            face_points[k] = point_list
    return face_points


def set_delaunay(image, points, get_sd=False):
    if len(points) == 0:
        return
    size = image.shape
    rect = (0, 0, size[1], size[0])
    sub_div = cv2.Subdiv2D(rect)
    if get_sd:
        return sub_div
    else:
        for i in points:
            sub_div.insert(i)
        triangle_list = sub_div.getTriangleList()
        size = image.shape
        r = (0, 0, size[1], size[0])

        def rect_contains(rec, point):
            if point[0] < rec[0]:
                return False
            elif point[1] < rec[1]:
                return False
            elif point[0] > rec[2]:
                return False
            elif point[1] > rec[3]:
                return False
            return True

        '''Draw triangles'''
        for t in triangle_list:
            pt1 = (t[0], t[1])
            pt2 = (t[2], t[3])
            pt3 = (t[4], t[5])
            if rect_contains(r, pt1) and rect_contains(r, pt2) and rect_contains(r, pt3):
                cv2.line(image, pt1, pt2, (255, 255, 255), 1, cv2.LINE_AA, 0)
                cv2.line(image, pt2, pt3, (255, 255, 255), 1, cv2.LINE_AA, 0)
                cv2.line(image, pt3, pt1, (255, 255, 255), 1, cv2.LINE_AA, 0)


def delaunay_file(fname):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    img = cv2.imread(fname)
    f_pts = get_points(img, detector, predictor)

    for k in f_pts.keys():
        set_delaunay(img, f_pts[k])
    cv2.imshow("img", img)
    cv2.waitKey(0)


def delaunay_image(img):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    f_pts = get_points(img, detector, predictor)

    for k in f_pts.keys():
        set_delaunay(img, f_pts[k])
    return img


def get_face_points(img):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    f_pts = get_points(img, detector, predictor)

    for k in f_pts.keys():
        for point in f_pts[k]:
            cv2.circle(img, point, 3, (255, 255, 255), -1)
    return img


def delaunay_camera():
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        pts = get_points(frame, detector, predictor)
        for k in pts.keys():
            set_delaunay(frame, pts[k])
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        cv2.imshow("frame", frame)

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    delaunay_camera()
    # delaunay_file("bill-clinton.jpg")
